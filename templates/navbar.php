<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">PATRICIA COSMETICS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <?php
        session_start();
        if (isset($_SESSION['name'])) {
          if($_SESSION['email'] == "admin@admin.com"){
      ?>
           <li class="nav-item">
            <a class="nav-link" href="../views/add_product.php">Add Product</a>
          </li>
      <?php
        }else{
      ?>
          <li class="nav-item">
            <a class="nav-link" href="../views/cart.php">Cart</a>
          </li>
      <?php
        }
      ?>

          <li class="nav-item">
            <a class="nav-link" href="../views/catalog.php">Best Sellers</a>
          </li>
          
           <li class="nav-item">
            <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="">Hello, <?php echo $_SESSION['name'] ?></a>
          </li>
      <?php
        }else{
      ?>
          <li class="nav-item">
            <a class="nav-link" href="../views/register.php">Register</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../views/login.php">Login</a>
          </li>
      <?php
        };
      ?>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>