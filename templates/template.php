<!DOCTYPE html>
<html>
<head>
	<title>json Ecommerce</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
	
</head>
<body class="bg-default">
	<?php require "navbar.php"?>
	<?php get_content() ?>
	<?php require "footer.php"?>

</body>
</html>
